# PRIMARSMELT
PRIMARSMELT is a software tool to calculate primary magmas from Mars.

## Name
PRIMARSMELT: Estimating Primary Magmas from Mars (https://doi.org/10.22002/w2ay2-dqs40)

## Description
PRIMARSMELT is a recalibration and update of the PRIMELT3 model for terrestrial primary magmas (Herzberg and Asimow, 2015), customized for Mars. PRIMARSMELT addresses both the mantle melting and magmatic differentiation aspects of the primary magma problem and only returns a solution when the forward and inverse approaches yield a consistent answer. PRIMARSMELT has been calibrated using available partial melting experiments on the most widely adopted estimate of the Martian mantle composition (Dreibus and Wänke, 1984). Its application to Martian basalts believed to have fractionated only olivine permits the derivation of primary magma compositions, melt fractions, initial melting pressures, and the mantle potential temperature (TP) in the source.

## Visuals
![PRIMARSMELT algorithm](image.png)

*Figure: Overview of the PRIMARSMELT algorithm.*

## Usage
PRIMARSMELT is freely available for download as a Microsoft® Excel® macro-enabled workbook and as a Python-based graphical user interface (GUI). The input data for PRIMARSMELT consist of a list of sample names, major oxide compositions (wt.%), iron oxidation states (i.e., Fe2+/ΣFe), and olivine fractionation pressures (in bars) for one or more primitive Martian basalts. Because the PRIMARSMELT algorithm is primarily based on olivine-liquid equilibria, the input composition must contain data for FeO and MgO contents; otherwise, the calculations will fail. The user can modify the mantle composition by updating the source FeO and MgO contents from the default values (i.e., DW and LF). Loading samples for calculations is done by simply typing the data in the Excel version or selecting a text file in the GUI.

Input structure:
Accepted file extensions are .csv and .txt files. The next lines are examples of the input format for two different samples. Data delimiters could be comma, semicolon, or tab. Oxides are in weight percent (wt%), Fe2Fet is the ferrous to total iron ratio (i.e., Fe+2/ΣFe), and the fractionation pressure P is given in bars. 

Examples:
Name	SiO2	TiO2	Al2O3	Cr2O3	FeO	MnO	MgO	CaO	Na2O	K2O	NiO	P2O5	Fe2Fet	P
MKea	48.3	2.64	13.15	0.0	11.056	0.18	8.05	13.18	2.28	0.74	0.0	0.311	0.9	1
Gorgona	46.54	0.7	11.9	0.25	11.21	0.18	17.82	10.09	1.11	0.04	0.1	0.05	0.9	1

Or
Name,SiO2,TiO2,Al2O3,Cr2O3,FeO,MnO,MgO,CaO,Na2O,K2O,NiO,P2O5,Fe2Fet,P
MKea,48.3,2.64,13.15,0.0,11.056,0.18,8.05,13.18,2.28,0.74,0.0,0.311,0.9,1
Gorgona,46.54,0.7,11.9,0.25,11.21,0.18,17.82,10.09,1.11,0.04,0.1,0.05,0.9,1

Or

Name;SiO2;TiO2;Al2O3;Cr2O3;FeO;MnO;MgO;CaO;Na2O;K2O;NiO;P2O5;Fe2Fet;P
MKea;48.3;2.64;13.15;0.0;11.056;0.18;8.05;13.18;2.28;0.74;0.0;0.311;0.9;1
Gorgona;46.54;0.7;11.9;0.25;11.21;0.18;17.82;10.09;1.11;0.04;0.1;0.05;0.9;1

A .csv file with bulk-rock compositions ready for PRIMARSMELT is available within the Windows and macOS folders.

Once the data are loaded, PRIMARSMELT performs the necessary calculations for all the samples in the input file. The output is presented as two separate blocks, giving the batch and AFM solutions. In the GUI, the first three columns in each block correspond to the sample name (Name), warnings related to the input composition (i.e., Warning: volatile-bearing source composition –VT; Warning: clinopyroxene fractionation – PX), and whether a primary magma solution exists (Solution: Y or N). The columns %Ol, Xfo, Mg#, and KD give the amount of olivine addition (or subtraction if negative) required for the primary magma solution, the forsterite composition of the olivine in equilibrium with the primary magma, the Mg# of the primary magma, and the value of the Fe2+-Mg distribution coefficient at the liquidus estimated from Toplis (2005) and used for olivine fractionation. Column P returns the (unmodified from the input) fractionation pressure and TLiq (°C) is the liquidus temperature at the fractionation pressure. Melt fractions are shown in columns F (Fe/Mg) for the batch solution and F AFM for the accumulated fractional melting solution. The mantle potential temperature in °C and initial melting pressure in GPa are given in columns TP (°C) and Pi (GPa). The remaining columns display the primary magma composition in weight percent of major element oxides. For the Excel version of PRIMARSMELT, the outputs are the same as described for PRIMELT3-P (Herzberg et al., 2023). It is noteworthy that some mismatch between the results of the Python and Excel versions of PRIMARSMELT may occur due to differences in the routines used to find the intersection between melt fractions for primary magma solutions. The majority of these discrepancies are subtle and result from interpolation between 1% increments of olivine addition/subtraction in Excel as opposed to the 0.1% increments used in Python. However, the Python GUI is often capable of identifying primary magma solutions in samples that the Excel version fails to detect.  Of the samples we have tested, the differences in the MgO content of the primary magma solutions calculated in Excel and the GUI were in general less than 0.2% MgO, which leads to small variations in Pf and Tp.

## Support
The versions of PRIMARSMELT included with the original manuscript are:

Microsoft Excel version of PRIMELT3-P (also known as MEGA)
PRIMELT3-P GUI for Windows (compiled in Windows 10, Intel Core i7 10th Gen, Python 3.9.15)
PRIMELT3-P GUI for macOS (compiled in macOS Ventura 13.0, Apple M2, Python 3.9.15)

The first time you run the GUI it may take some time to start. It has been tested on different Windows and MAcOS versions and should run well in machines using both Intel and M chips. If you experience any problems, cannot start the program, or need a new compilation that runs in older/newer versions of Windows/macOS, please visit Gitlab (https://gitlab.com/primelt_software/primelt3-p) or send me an email to jdhernandez@caltech.edu.

If you have problems opening the Excel version due to Microsoft Office blocking macros after an update, please follow the following instructions:

1. Close the workbook.
2. Right-click on the workbook.
3. Select Properties.
4. Under the General tab, make sure to check the Unblock box in Security.
5. Hit the Apply button.
6. Now open the workbook.
7. You will see the error message has now disappeared.

## Roadmap
Expected changes in newer versions include an option to constrain Fe3+/ΣFe ratios from user-defined fO2 conditions and additional parameterizations extended to other source compositions and liquids that have fractionated clinopyroxene and plagioclase, in addition to olivine. 

## License
This project is licensed under the MIT License

